"""
Reads Solr-add XML and writes output ready for import into CorText Manager
(manager.cortext.net)
"""

import codecs
from lxml import etree
import os
import re


indir = "/home/pablo/projects/clm/enbxml"
outdir= "/home/pablo/projects/clm/enbcor"
header = u"title\tdate\tvol\tcop\tcity\ttext\n"


def read_xml(fn):
    tree = etree.parse(fn)
    # metadata
    title = tree.xpath("//field[@name='title']/text()")[0]
    date = tree.xpath("//field[@name='date']/text()")[0][0:4]
    vol = tree.xpath("//field[@name='vol']/text()")[0]
    cop = tree.xpath("//field[@name='cop']/text()")[0]
    city = tree.xpath("//field[@name='city']/text()")[0]
    country = tree.xpath("//field[@name='country']/text()")[0]
    # text
    rtext = tree.xpath("//field[@name='description']/text()")[0]
    ctext = re.sub("\s*<p />\s*", "***", rtext)
    return {"ti": title, "da": date, "vo": vol,
            "cop": cop, "ci": city, "co": country, "te": ctext}


def read_dir(indir):
    for fn in os.listdir(indir):
        ffn = os.path.join(indir, fn)
        print "- Reading {}".format(ffn)
        yield fn, read_xml(ffn)


def write_file(outdir, fn, infos):
    ffn = os.path.join(outdir, fn)
    print "  > Writing {}".format(ffn)
    with codecs.open(ffn, "w", "utf8") as ofd:
        ofd.write(header)
        ofd.write("\t".join([infos["ti"], infos["da"], infos["vo"],
                             infos["cop"], infos["ci"], infos["te"]]))


def main():
    if not os.path.exists(outdir):
        os.makedirs(outdir)
    for fn, data in read_dir(indir):
        write_file(outdir, fn, data)


if __name__ == "__main__":
    main()
