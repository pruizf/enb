The cleaned [ENB vol. 12](http://enb.iisd.org/enb/vol12/) reports for Conference of the Parties (COP) summits are here (258 reports). The directories have the following content:

* **clean_txt**: plain text
* **clean_xml**: XML format that can be indexed in a Solr index with [update requests](https://wiki.apache.org/solr/UpdateXmlMessages)
* **crawled**: the files (text or html) as originally crawled from the [ENB site](http://enb.iisd.org/enb/vol12/)

* **clean_txt_new**: plain text for issues after COP-20
* **crawled_new**: html crawled for issues after COP-20

Metadata for the files is also available in **document_metadata.txt**. It was extracted from the table of contents for the volume.

Metadata includes, for each filename, the issue number, the date for the issue, the COP and COP location, and whether it is a daily issue of a summary for the whole COP
