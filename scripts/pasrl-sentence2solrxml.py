"""
From PASRL delimited output (i.e. has sentences) to Solr XML.
So that can index corpus at sentence level in Solr.
"""

__author__ = 'Pablo Ruiz'
__date__ = '19/08/17'
__email__ = 'pabloruizfabo@gmail.com'


import codecs
from lxml import etree
import os
import re
import sys


# IO
try:
    inf = sys.argv[1]
except IndexError:
    inf = "/home/pablo/projects/ie/out/pasrl/enb_after_cop_20_from_pickle_accept_incomplete_with_actor_types.txt"

try:
    oud = sys.argv[2]
except IndexError:
    oud = "/home/pablo/projects/clm/enb_corpus/sentences/enb_cops_after_20_sentences_solr_format"

# Const
fnre = re.compile(ur"^(enb[0-9]+e?\.html-[0-9]+)\t")


def get_sentences(fn):
    sents = {}
    with codecs.open(fn, "r", "utf8") as fd:
        line = fd.readline()
        while line:
            has_sent = re.match(fnre, line)
            if has_sent:
                sl = line.strip().split("\t")
                # line: idTABsentence
                assert sl[0] not in sents
                sents[sl[0]] = sl[1]
            line = fd.readline()
    return sents


def sent2xml(sentdico, odir):
    """Given dico returned by L{get_sentences} for a series of files,
       write each sentence in Solr update XML format"""
    print "== Creating XML =="
    for sid, sent in sentdico.items():
        # create xml elements
        root = etree.Element("add")
        doc = etree.SubElement(root, "doc")
        id_ = etree.SubElement(doc, "field", name="id")
        title = etree.SubElement(doc, "field", name="title")
        text = etree.SubElement(doc, "field", name="description")
        # set values
        id_.text, title.text = sid, sid
        text.text = etree.CDATA(sent)
        # serialize
        sertree = etree.tostring(root, xml_declaration=True,
                                 encoding="utf8", pretty_print=True)
        ofn = os.path.join(odir, "{}.xml".format(sid))
        with open(ofn, "w") as ofh:
            ofh.write(sertree)


def main(infi, odir):
    print u"IN: {}".format(infi)
    print u"OU: {}".format(odir)
    mysents = get_sentences(infi)
    sent2xml(mysents, odir)


if __name__ == "__main__":
    main(inf, oud)