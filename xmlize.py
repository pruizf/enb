"""Transforms ENB vol 12 COP reports in text format into Solr XML format"""

import codecs
import inspect
from lxml import etree
import os
import re
import sys


# txtdata = "/home/pablo/projects/clm/enbmid11000"
# outdata = txtdata.replace("mid", "xml")
txtdata = "/home/pablo/projects/clm/enb/out/clean_txt_new"
outdata = txtdata.replace("txt", "xml")
metadata = "/home/pablo/projects/clm/enb/data/copdates.txt"
# url patterns
URLPAT_TXT = "http://www.iisd.ca/download/asc/enb12{}e.txt"
URLPAT_HTML = "http://www.iisd.ca/vol12/enb12{}e.html"


here = os.path.dirname(os.path.abspath(
    inspect.getfile(inspect.currentframe())))
sys.path.append(here)

import utils


def get_metadata():
    """Get cop metadata from file listing cop locations and dates"""
    print "Getting metadata"
    mdd = {}
    with codecs.open(metadata, "r", "utf8") as md:
        line = md.readline()
        while line:
            sl = line.strip().split("\t")
            mdd[int(sl[0])] = {"date": sl[1], "city": sl[2], "country": sl[3]}
            line = md.readline()
    return mdd


def find_text(fn):
    """Given filename, return {volnbr: (volume text, volume filename,
       volume_url)}"""
    vol2txt = {}
    volnbr = int(re.search("enb12([0-9]+)e?\.(?:txt|html)", fn).group(1))
    if volnbr <= 295 and volnbr != 120:
        orig_url = URLPAT_TXT.format(volnbr)
    else:
        orig_url = URLPAT_HTML.format(volnbr)
    with codecs.open(fn, "r", "utf8") as myfn:
        vol2txt[volnbr] = (myfn.read(), os.path.basename(fn), orig_url)
    return vol2txt


def text2xml(textdico, mdata, copnames, odir):
    """Given dico of returned by L{find_text} for a series of files,
       write the docs in Solr XML format"""
    print "== Creating XML =="
    for vol, infos in textdico.items():
        print infos[1],
        # create xml elements
        root = etree.Element("add")
        doc = etree.SubElement(root, "doc")
        title = etree.SubElement(doc, "field", name="title")
        vid = etree.SubElement(doc, "field", name="id")
        copname = etree.SubElement(doc, "field", name="cop")
        text = etree.SubElement(doc, "field", name="description")
        url = etree.SubElement(doc, "field", name="url") #url for Velocity
        original_url = etree.SubElement(doc, "field", name="original_url")
        volnbr = etree.SubElement(doc, "field", name="vol")
        date = etree.SubElement(doc, "field", name="date")
        city = etree.SubElement(doc, "field", name="city")
        country = etree.SubElement(doc, "field", name="country")
        # set values
        vid.text = infos[1]
        title.text = "Nbr {} - {}".format(vol, copnames[vol])
        text.text = etree.CDATA(infos[0].strip().replace("\n", "<p />"))
        print copnames[vol]
        # to make cop facet sort properly by name and fit the template column
        if "INC" in copnames[vol]:
            copname.text = " - ".join(("00", "INC-11", "NY"))
        else:
            copname.text = re.sub(r"^[^-]+-([0-9]{2})", "\g<1>",
                                  " - ".join((copnames[vol],
                                              mdata[vol]["city"])))
        url.text = "http://localhost:8983/solr/enb12/browse?q=id:{0}".format(
            infos[1].replace(" ", "_"))
        original_url.text = infos[2]
        volnbr.text = unicode(vol)
        city.text = mdata[vol]["city"]
        country.text = mdata[vol]["country"]
        # Solr format date
        solrdate = utils.date2solr(mdata[vol]["date"])
        if not solrdate:
            print "!! BAD DATE: {}".format(infos[1])
            solrdate = "1000-01-01:T00:00:00Z"
        date.text = solrdate
        # serialize
        sertree = etree.tostring(root, xml_declaration=True,
                                 encoding="utf8", pretty_print=True)
        ofn = os.path.join(odir, os.path.splitext(infos[1])[0] + ".xml")
        with open(ofn, "w") as ofh:
            print "=> {}".format(os.path.basename(ofn))
            ofh.write(sertree)


def main(indir=txtdata, outdir=outdata):
    global volnbr2info
    global volnbr2infos
    if not os.path.exists(outdir):
        os.makedirs(outdir)
    mdata = get_metadata()
    cop_names = utils.find_cop_name(utils.VOLS)
    volnbr2infos = {}
    for fn in os.listdir(indir):
        # skip index
        if fn.startswith("index_enb"):
            continue
        volnbr2info = find_text(os.path.join(indir, fn))
        volnbr2infos.update(volnbr2info)
    text2xml(volnbr2infos, mdata, cop_names, outdir)
    print "> Indir: {}".format(indir)
    print "> Outdir: {}".format(outdir)


if __name__ == "__main__":
    main()