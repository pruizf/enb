# -*- coding: utf8 -*-

"""General utils"""

from bs4 import BeautifulSoup as BsP
from datetime import datetime as dt
import requests
import re
import string


# ranges of volumes for each COP
VOLS = """1-11,12-21,28-38,67-76,87-97,112-123,152-163,
          #166-176,178-189,201-209,221-231,249-260,#280-291,
          #307-318,343-354,385-395,448-459,487-498,523-534,
          556-567,583-594,608-626,652-663,678-689"""


MONTHS = ("(?:January|February|March|April|May|June|July|"
          "August|September|October|November|December)")


def find_cop_name(volstr):
    """
    Get COP number for an ENB volume based on manually collected info
    about volume number to COP-name mapping.
    @note: volumes starting with '#' in volstr have an additional name
    (besides 'COP'), see ENB vol 12 for details.
    """
    vol2cop = {}
    volranges = re.sub("\s", "", volstr)
    volranges = [x.replace("#", "") for x in volranges.split(",")]
    # starts at 0 cos cop-0 is INC-11
    dones = 0
    for vr in volranges:
        beg, end = [int(nbr) for nbr in vr.split("-")]
        for vol in range(beg, end + 1):
            if vol <= 11:
                vol2cop[vol] = "0-INC-11"
            elif vol < 166:
                vol2cop[vol] = "COP-{}".format(string.zfill(dones, 2))
            elif vol >= 166 and vol <= 176:
                vol2cop[vol] = "COP-06bis"
            elif vol > 176 and vol < 620:
                if dones -1 == 20:
                    vol2cop[vol] = "COP-20a"
                else:
                    vol2cop[vol] = "COP-{}".format(string.zfill(dones - 1, 2))
            elif vol >= 620 and vol <= 626:
                #if dones == 20:
                vol2cop[vol] = "COP-20b"
                #else:
                #    vol2cop[vol] = "COP-{}".format(string.zfill(dones - 1, 2))
            else:
                vol2cop[vol] = "COP-{}".format(string.zfill(dones - 1, 2))
        dones += 1
    return vol2cop


def check_digit_format(date):
    """Check if initial digit of date is two-digit format"""
    needs_zfill = re.match("^[0-9][ -]", date)
    if needs_zfill:
        date = "".join(("0", date))
    return date


def get_cop_date_and_place():
    """
    Based on the vol12 cover page, get dates and places
    for each COP
    @return: list with infos by volume number
    """

    url = "http://www.iisd.ca/vol12/"

    r = requests.get(url)
    txt = r.content.decode("utf8")

    soup = BsP(txt, 'html.parser')

    allinfos = []

    print "Looking for COP dates"
    rows = soup.findAll('tr')
    for row in rows:
        keep = False
        cells = row.findAll('td')
        for cell in cells:
            # using INC-11 as 'COP-0'
            if (cell.get_text().find("COP") > 0 or
                cell.get_text().find("INC") > 0):
                keep = True
                break
        if keep:
            infos = []
            for cell in cells:
                celltxt = cell.get_text()
                hasvol = re.search(r"Volume 12 Number ([0-9]+)", celltxt)
                if hasvol:
                    vol = int(hasvol.group(1))
                    infos.append(vol)
                # no linebreaks between date parts
                hasdate = re.search(
                    re.compile(r"""([0-9]{0,2}                     # number first
                                      [^\n]*%s                     # month
                                      [^\n]+[0-9]{4})              # year (no \n)
                                    \s*
                                    (.+)(?:\W|$)""" % MONTHS,      # place
                               re.DOTALL|re.MULTILINE|re.VERBOSE),
                    celltxt)
                if hasdate:
                    date = re.sub(r"\s+", " ", hasdate.group(1))
                    date = check_digit_format(date)
                    place = hasdate.group(2).strip()
                    placebits = re.match(r"^([^,]+), (.+)$", place)
                    if placebits:
                        city = placebits.group(1).strip()
                        country = placebits.group(2).strip()
                    infos.extend((date, city, country))
                else:
                    # some COPs have linebreaks between date parts
                    hasdate = re.search(
                        re.compile(r"""([0-9]{0,2}                 # number first
                                          [^\n]*%s                 # month
                                          .+[0-9]{4})              # year (anything)
                                       (.+)(?:\W|$)""" % MONTHS,   # place
                                   re.DOTALL|re.MULTILINE|re.VERBOSE),
                        celltxt)
                    if hasdate:
                        date = re.sub(r"\s+", " ", hasdate.group(1))
                        date = check_digit_format(date)
                        place = hasdate.group(2).strip()
                        placebits = re.match(re.compile(r"^([^,]+), (.+)$",
                                                        re.DOTALL), place)
                        if placebits:
                            city = re.sub("\s+", " ",
                                          placebits.group(1).strip())
                            country = re.sub("\s+", " ",
                                             placebits.group(2).strip())
                        infos.extend((date, city, country))
                # if False:
                #     print "###", date, "###", "$$", re.sub(
                #         "\s+", " ", place), "$$", city, "!!", country
            if infos:
                allinfos.append("\t".join([unicode(i) for i in infos]))
    return allinfos


def correct_other_text_errors(txt, delete_errors=False):
    """Correct other errors (even browsers were displaying
       this characters incorrectly)"""
    txt = txt.replace(u"visï¿½vis", "vis-a-vis")
    txt = re.sub(u"ï¿½s(\s+|$)", "'s\g<1>", txt)  # genitive
    txt = re.sub(u"(^|\s+)ï¿½", '\g<1>``', txt)   # l quot
    txt = re.sub(u"ï¿½( |$)", '"\g<1>', txt)        # r quot
    txt = re.sub(u"nï¿½t", "n't", txt)              # contractions
    txt = re.sub(u"Iï¿½m", "I'm", txt)
    txt = re.sub(u"ï¿½ll", "'ll", txt)
    txt = re.sub(u"ï¿½re", "'re", txt)
    txt = re.sub(u"([0-9]+)ï¿½\s*C", u"\g<1>\u00b0C", txt)  # degree symbol
    if delete_errors:
        txt = re.sub(u"ï¿½", u"", txt)
    return txt


def date2solr(mydate):
    """
    Return a date formatted so that solr can accept it
    @note: returns False when can't format date!
    """
    orig = mydate
    if "-" in mydate:
        for1 = re.match("([0-9]{1,2})-([0-9]{1,2})[ ](%s)" % MONTHS, mydate)
        for2 = re.match("(%s) ([0-9]{1,2})[ ]?-[ ]?([0-9]{1,2})" % MONTHS,
                        mydate)
        for3 = re.match("(%s) ([0-9]{1,2})[ ]?-[ ]?(%s)[ ]([0-9]{1,2})" % (
            MONTHS, MONTHS), mydate)
        for4 = re.match("([0-9]{1,2})[ ](%s)[ ]?-[ ]?([0-9]{1,2})[ ](%s)" % (
            MONTHS, MONTHS), mydate)
        for5 = re.match("([0-9]{4})-([0-9]{2})-([0-9]{2})", mydate)
        year = re.search("([0-9]{4})", mydate).group(1)
        # use second date only
        if for1:
            d1 = for1.group(1)
            d2 = for1.group(2)
            month = for1.group(3)
        elif for2:
            d1 = for2.group(2)
            d2 = for2.group(3)
            month = for2.group(1)
        elif for3:
            d1 = for3.group(2)
            d2 = for3.group(4)
            m1 = for3.group(1)
            m2 = for3.group(3)
        elif for4:
            d2 = for4.group(3)
            m2 = for4.group(4)
        elif for5:
            d2 = for5.group(3)
            m2 = for5.group(2)
        else:
            print "!!! BAD DATE for {}".format(mydate)
            return False
        try:
            mydate = u"{} {}, {}".format(m2, d2, year)
        except NameError:
            mydate = u"{} {}, {}".format(month, d2, year)
        try:
            dob = dt.strptime(mydate, '%B %d, %Y')
        except ValueError:
            # cop 21 and 22
            dob = dt.strptime(mydate, '%m %d, %Y')
    elif mydate[0].isdigit():
        dob = dt.strptime(mydate, '%d %B %Y')
    else:
        dob = dt.strptime(mydate, '%B %d, %Y')
    return dob.strftime("%Y-%m-%dT00:00:00Z")


if __name__ == "__main__":
    import inspect
    import os
    here = os.path.dirname(os.path.abspath(
        inspect.getfile(inspect.currentframe())))
    if True:
        import codecs
        outfn = os.path.join(here,"data/copdates.txt")
        with codecs.open(outfn, "w", "utf8") as out:
            outstr = "\n".join(get_cop_date_and_place())
            print "Writing out"
            out.write(outstr)
        print "Done"
    if False:
        infn = os.path.join(here, "data/copdates.txt")
        for ll in open(infn):
            sl = ll.split("\t")
            print u"{}\t{}".format(sl[1], date2solr(sl[1]))
