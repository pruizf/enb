"""Crawls missing COP issues for ENB vol 12"""

__author__ = 'Pablo Ruiz'
__date__ = '07/08/17'
__email__ = 'pabloruizfabo@gmail.com'


from bs4 import BeautifulSoup as bs
import codecs
from datetime import datetime as dt
from datetime import timedelta
from ftfy import fix_text  # fixes encoding errors
import os
import re
import requests
import sys


# io
try:
    crawled = sys.argv[1]
except IndexError:
    crawled = "/home/pablo/projects/clm/enb/out/crawled_new"
try:
    cleaned = sys.argv[2]
except IndexError:
    cleaned = os.path.split(crawled)[0] + os.sep + "clean_txt_new"
try:
    metadatafn = sys.argv[3]
except IndexError:
    metadatafn = "/home/pablo/projects/ie/uibo/webuibo/ext_data/txt/document_metadata.txt"


# constants
CRAWL_AND_CLEANUP = False  # False if only want to add metadata, True if need crawl and cleanup
urlpat = "http://enb.iisd.org/vol12/enb12{}e.html"
ranges = [(652, 663, "21", "Paris"), (678, 689, "22", "Marrakech")]


def crawl_range(rge):
    print "Crawling"
    nbr2text = {}
    for nbr in range(rge[0], rge[1] + 1):
        text = requests.get(urlpat.format(nbr)).text
        nbr2text[r"enb12{}.html".format(nbr)] = text
    return nbr2text


def remove_spaces(txt):
    txt = re.sub(ur" {2,}", " ", txt)
    txt = re.sub(ur" \n", "\n", txt)
    txt = re.sub(ur"\n ", "\n", txt)
    return txt


def cleanup(txt):
    print "Cleaning up"
    soup = bs(txt, 'html.parser')
    # page titles
    titles = []
    for ttl_tag in soup.findAll("h3", {"class": "meeting_date1"}):
        titles.append(ttl_tag.get_text())
    # text
    body_text = []
    for txt_tag in soup.findAll(["div", "table"]):
        if "class" in txt_tag.attrs:
            for attr in txt_tag.attrs["class"]:
                if attr == "webtext":
                    body_text.append(remove_spaces(txt_tag.get_text()))
    outstr = "\n".join(["\n".join(titles), "\n".join(body_text)])
    return outstr


def collect_metadata_from_clean_texts(cleandir, myranges):
    fn2date = {}
    for fn in os.listdir(cleandir):
        volnbr = re.match("enb12([0-9]+).html", fn).group(1)
        ffn = os.path.join(cleandir, fn)
        with codecs.open(ffn, "r", "utf8") as ifd:
            txt = ifd.read()
            #Example: Volume 12 Number 652 | Monday, 30 November 2015
            dateltr = re.search(
                ur"Volume 12 Number ([0-9]+) \| ([^,]+), ([0-9]+) (\w+) ([0-9]+)", txt)
            try:
                assert dateltr
            except AssertionError:
                print ur"Text has no date: [{}] [{}]".format(fn, txt)
                continue
            mydate = u"{} {}, {}".format(
                dateltr.group(4), dateltr.group(3), dateltr.group(5))
            dob = dt.strptime(mydate, '%B %d, %Y')
            # in Paris COP dates on issue are negotiation date + 1
            if int(volnbr) < 664:
                dob = dob - timedelta(days=1)
            filedate = dob.strftime("%Y-%m-%d")
            if int(volnbr) in [rng[1] for rng in myranges]:
                fn2date[fn] = [filedate, volnbr, "summary"]
            else:
                fn2date[fn] = [filedate, volnbr, "daily"]
            if int(volnbr) < 664:
                fn2date[fn].extend(myranges[0][-2:])
            else:
                fn2date[fn].extend(myranges[1][-2:])
    return fn2date


if __name__ == "__main__":
    if CRAWL_AND_CLEANUP:
        d1 = crawl_range(ranges[0])
        d2 = crawl_range(ranges[1])
        d1.update(d2)
        dclean = {}
        for mydir in crawled, cleaned:
            if not os.path.exists(mydir):
                os.makedirs(mydir)
        print "Writing crawled html"
        for ke, va in d1.items():
            with codecs.open(os.path.join(crawled, ke), "w", "utf8") as of:
                of.write(fix_text(va))
            dclean[ke] = cleanup(va)
        print "Writing clean text"
        for keclean, vaclean in dclean.items():
            with codecs.open(os.path.join(cleaned, keclean), "w", "utf8") as cleanof:
                cleanof.write(fix_text(vaclean))
    print "Getting metadata (dates) for fields"
    fname2date = collect_metadata_from_clean_texts(cleaned, ranges)
    # write out new metadata
    print ur"Writing new metadata to {}".format(metadatafn)
    md2add = []
    with codecs.open(metadatafn, "a+", "utf8") as mdf:
        txt = mdf.read()
        if txt[-1] != "\n":
            md2add.append("\n")
        for fn, (myfiledate, myvolnbr, issue_type, copnbr,
                 city) in sorted(fname2date.items()):
            outl = "\t".join([fn, myvolnbr, copnbr, city, myfiledate, issue_type])
            md2add.append(outl)
        mdf.write("\n".join(md2add))
            
