# -*- coding: utf8 -*-

"""
To clean up ENB vol 12 COP reports, writing out as text
"""

from bs4 import BeautifulSoup as bs
import codecs
import re

import utils


# section titles
START_BODY = ur"^\s*Vol. 12 No. [0-9]+(?:\s+[0-9]+\s)?"  # good cue, covers the 143 text files
                                                         # earlier than vol 296

# For enb1201e.txt make the ?: group optional
END_BODY = re.compile(
    ur"^\s*This issue of the Earth\s(?:Negotiations Bulletin)?")

# For HTML files
END_BODY_HTML = re.compile(
    ur"This\s+issue of the (?:<em>)?Earth Negotiations Bulletin")


SECTION = ur"^([A-Z\s\[\]{}!#$%&\'()\*\+,./:;<=>?@^_`-]+)$"


def raw_text_to_xml(vol):
    """
    Transforms raw text volumes into an XML format.
    @param vol: full path to filename for volume
    @note:
      - Applies to volumes below 296 (but not vol. 120)
      - Assumes this pattern in file names: "enb12{}e.txt".format(vol)
    """
    if ".html" in vol:
        return False
    try:
        assert int(os.path.split(
            vol.replace("enb12", "").replace("e.txt", ""))[1]) <= 295
        assert int(os.path.split(
            vol.replace("enb12", "").replace("e.txt", ""))[1]) != 120
    except AssertionError:
        #print "This is for volumes below 295, excepting volume 120"
        return False
    with codecs.open(vol, "r", "utf8") as inf:
        body = extract_txt_body(inf)
    return body


def html_to_xml_1(vol):
    """
    Transforms raw text volumes into an XML format.
    @param vol: full path to filename for volume
    @note:
      - Applies to volumes between 297 and 384
      - Assumes this pattern in file names: "enb12{}e.html".format(vol)
    """
    if ".txt" in vol:
        return False
    volint = int(os.path.split(
        vol.replace("enb12", "").replace("e.html", ""))[1])
    try:
        assert (296 <= volint and volint <= 384) or volint == 120
    except AssertionError:
        #print "This is for volumes between 296 and 384"
        return ""
    #print "- Processing: {}".format(vol)
    with codecs.open(vol, "r", "utf8") as inf:
        body = extract_html_body_1(inf)
    return body


def html_to_xml_2(vol):
    """
    Transforms raw text volumes into an XML format.
    @param vol: full path to filename for volume
    @note:
      - Applies to volumes later than 384
      - Assumes this pattern in file names: "enb12{}e.html".format(vol)
    """
    if ".txt" in vol:
        return False
    volint = int(os.path.split(
        vol.replace("enb12", "").replace("e.html", ""))[1])
    try:
        assert volint >= 385
    except AssertionError:
        #print "This is for volumes above 385"
        return ""
    p_in_div = False # flag for special treatment of some file
    with codecs.open(vol, "r", "utf8") as inf:
        if volint >= 385 and volint <= 394:
            p_in_div = True
        body = extract_html_body_2(
            inf, p_in_div=p_in_div)
    return body


def extract_txt_body(fo):
    """
    Collect lines other than header and footer boilerplate.
    @return: List of body lines.
    @rtype: list
    @note: Applies to early COPs in TXT format.
    """
    body = []
    line = fo.readline()
    while line:
        if re.search(START_BODY, line):
            while line:
                if not re.match(END_BODY, line):
                    body.append(line)
                else:
                    break
                line = fo.readline()
        line = fo.readline()
    alltext = "".join(body)
    nor = normalize_text(alltext)
    return "".join(nor)


def extract_html_body_1(fo):
    """
    Collect lines other than header and footer boilerplate.
    @return:
    @rtype: list
    @note: Applies to early COPs in HTML format (<= vol 384).
    """
    soup = bs(fo, 'html.parser')
    titles = []
    for tag in soup.findAll("p", {"class": 'ENB-Headline2'}):
        raw_title = tag.get_text()
        nor_title = normalize_text(raw_title)
        nor_title = normalize_text_2(nor_title)
        titles.append(nor_title)
    body = []
    for tag in soup.findAll("p"):
        try:
            for attr in tag.attrs['class']:
                if attr in ('ENB-Body', 'ENB-Headline3', 'MsoNormal'):
                    raw = tag.get_text()
                    nor = normalize_text(raw)
                    nor = normalize_text_2(nor)
                    if nor:
                        body.append(nor)
                    break
        except KeyError:
            continue
    alltitles = "\n".join(titles)
    alltext = re.sub("\n{2,}", "\n", "\n".join(body))
    outstr = "\n".join([alltitles, alltext])
    if outstr != "\n":
        return outstr
    else:
        return ""


def is_boilerplate(html):
    """Some cases of boilerplate in HTML volumes"""
    if re.search(END_BODY_HTML, html):
        return True
    elif "^ up to top" in html:
        return True
    elif "| Back to IISD RS " in html:
        return True
    else:
        return False


def extract_html_body_2(fo, p_in_div=False):
    """
    Collect lines other than header and footer boilerplate.
    @param fo: file object for html file
    @param p_in_div: some files where the 'p' that require a
    linebreak after are under a 'div' (generally they are under a 'td')
    @note: Applies to later COPs in HTML format (> vol 384).
    """
    soup = bs(fo, 'html.parser')
    titles = []
    for tag in soup.findAll("p", {"class": ['greyheading',
                                            'greyheading1',
                                            'meeting_title']}):
        raw_title = tag.get_text()
        nor_title = normalize_text(raw_title)
        nor_title = normalize_text_2(nor_title)
        titles.append(nor_title)
    body = []
    for tag in soup.findAll(["td", "div", "p"]):
        try:
            for attr in tag.attrs['class']:
                if attr in ('textstory', 'textstory1'):
                    # td (e.g. vol 623): text is in p-children
                    # treat separately: avoid all text on same line
                    # for volumes 385 to 394 (p_in_div = True)
                    # it's a div not a td that has the p children
                    if (tag.name == "td") or \
                            (p_in_div and tag.name == "div"):
                        for par in tag.findAll("p"):
                            raw = par.get_text()
                            if p_in_div:
                                raw += "\n"
                            if is_boilerplate(raw):
                                continue
                            nor = normalize_text(raw)
                            nor = normalize_text_2(nor)
                            body.append(nor)
                    # div and p
                    else:
                        raw = tag.get_text()
                        if is_boilerplate(raw):
                            continue
                        nor = normalize_text(raw)
                        nor = normalize_text_2(nor)
                        body.append(nor)
                    # skip other classes than 'textstory1?'
                    break
        except KeyError:
            continue
    alltitle = "\n".join(titles)
    alltext = re.sub("\n{2,}", "\n", "\n".join(body))
    outstr = "\n".join([alltitle, alltext])
    if outstr != "\n":
        return outstr
    else:
        return ""


def normalize_text(text):
    """
    Render text better formatted for NLP. Eliminate hard breaks etc.
    """
    # expressions -------------------------------------------------------------
    HBX = ur"(?<!\n)\n"      # hard breaks (that do not represent a paragraph break)
                             # are not preceded by another break
    KEEPHY = re.compile(ur"""((?:co|non|high|sub|
                             one|two|three|four|five|
                             six|seven|eight|nine|ten)-)\n""", re.VERBOSE|
                                                               re.IGNORECASE)
    DELHY = ur"-\n"
    FIXHY = re.compile(ur"([a-z]-) ")
    # procedure ---------------------------------------------------------------
    # hard breaks
    text = re.sub("\r\n", "\n", text)
    text = re.sub(HBX, " ", text)
    # hyphens
    text = re.sub(KEEPHY, "\g<1>", text)
    text = re.sub(DELHY, "", text)
    text = re.sub(FIXHY, "\g<1>", text)
    # characters
    # TODO is this obsolete with current way to decode?
    text = re.sub(u"\u0092", "'", text)  # apostrophe
    text = re.sub(u"\u0093", u'``', text)  # opening quotes
    text = re.sub(u"\u0094", u'"', text)  # closing quotes
    text = re.sub(u"\u0096", u'–', text)  # en-dash
    text = re.sub(u"\u0097", u'–', text)  # en-dash
    # other whitespace
    text = re.sub(r"[ ]{2,}", " ", text)
    # replace 'smart quotes'
    # text = re.sub(u"“", "\"", text) # did double ones with Perl
    # text = re.sub(u"”", "\"", text)
    text = re.sub(u"’", "'", text)
    text = re.sub(u"‘", "'", text)
    text = text.replace("Rau'l", u"Raúl")
    return text


def normalize_text_2(text):
    """
    Additional normalization for html-sources between vols 296 and 384.
    """
    text = re.sub(r"[ \t]{2,}", " ", text)
    text = re.sub(r"[ \t]+\n", "\n", text)
    text = re.sub("\n{2,}", "\n", text)
    text = re.sub(re.compile(r"^\s+", re.M), "", text)
    text = re.sub("\xe2", "", text)  #TODO: applies now?
    return text


# TESTS
if __name__ == "__main__":
    import os
    #indir = "/home/pablo/projects/clm/enbraw11000"
    indir = "/home/pablo/projects/clm/enb/out/crawled"
    #testsdir = "/home/pablo/projects/clm/enb/out/clean_new_14"
    testsdir = "/home/pablo/projects/clm/enb/out/clean_txt"
    if not os.path.exists(testsdir):
        os.makedirs(testsdir)
    fn2xml = {}
    for fn in sorted(os.listdir(indir)):
        print "- Processing: {}".format(fn)
        ffn = os.path.join(indir, fn)
        # text-based
        txt = raw_text_to_xml(ffn)
        if txt:
            fn2xml[fn] = txt
        else:
            # html-based format 1
            txt = html_to_xml_1(ffn)
            if txt:
                fn2xml[fn] = txt
            else:
                # html-based format 2
                txt = html_to_xml_2(ffn)
                if txt:
                    fn2xml[fn] = txt
                else:
                    fn2xml[fn] = ""
                    print "  ! Empty: {}".format(fn)
        print "  > Writing: {}".format(fn)
        with codecs.open(os.path.join(testsdir, fn), "w", "utf8") as out:
            mytext = fn2xml[fn]
            outtext = utils.correct_other_text_errors(mytext,
                                                 delete_errors=True)
            out.write(outtext)
    print "\nFinal corrections with Perl"
    os.system("perl -i -pe 's/“/\"/g' {}/*".format(testsdir))
    os.system("perl -i -pe 's/”/\"/g' {}/*".format(testsdir))

    print "Output dir: {}".format(testsdir)