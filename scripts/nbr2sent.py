import codecs
import inspect
from nltk import sent_tokenize
import os
import sys

here = os.path.dirname(os.path.abspath(
    inspect.getfile(inspect.currentframe())))
sys.path.append(here)


indir = os.path.join(here,"../out/clean_txt")
outdir = os.path.join(here, "../../work/sentences_txt")


def sentence_split(ind, outd):
    for ffn in sorted([os.path.join(indir, fn) for fn in os.listdir(ind)]):
        outfn = os.path.join(outd, os.path.basename(ffn))
        print ffn
        with codecs.open(ffn, "r", "utf8") as inf, \
             codecs.open(outfn, "w", "utf8") as outf:
            txt = inf.read()
            sents = sent_tokenize(txt)
            outf.write("\n".join(sents))


def main():
    if not os.path.exists(outdir):
        os.makedirs(outdir)
        sentence_split(indir, outdir)


if __name__ == "__main__":
    main()