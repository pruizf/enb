Contains (modified) configuration (or other) files that can be copied over to the Solr 4 app.

The relevant directories in a Solr 4 app are:

 - solr (index configuration and writeout templates)
 - solr-webapp (configuration for webapp itself)


