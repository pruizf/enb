"""To create corpus split per sentence, with date etc. Default sentence splitter"""

import codecs
import inspect
from lxml import etree
from nltk import sent_tokenize
import os
import re
import string
import sys


here = os.path.dirname(os.path.abspath(
    inspect.getfile(inspect.currentframe())))
sys.path.append(here)


indir = "/home/pablo/projects/clm/enbxml11000"


def xml2sents(inf):
    print os.path.basename(inf)
    all_out = []
    tree = etree.parse(inf)
    txt = tree.xpath("//field[@name='description']/text()")[0]
    txt = re.sub(r"\s*<p />\s*", "\n", txt)
    txt = re.sub("\.\.", ".", txt)
    sents = sent_tokenize(txt)
    new_sents = []
    for sent in sents:
        new_sents.extend(sent.split("\n"))
    nbr = string.zfill(tree.xpath(
        "//field[@name='vol']/text()")[0], 3)
    cop = tree.xpath("//field[@name='cop']/text()")[0]
    date = tree.xpath("//field[@name='date']/text()")[0][0:4]
    for sent in new_sents:
        out = [nbr, cop, date, sent]
        all_out.append("\t".join([unicode(x) for x in out]))
    return {int(nbr): all_out}


def main(mydir):
    all_fsents = {}
    try:
        outf = sys.argv[1]
    except IndexError:
        outf = os.path.join(here, "../../work/cop_sentences_2.txt")
    if os.path.isfile(outf):
        os.remove(outf)
        print "Deleted old version of {}".format(os.path.normpath(outf))
    for ffn in sorted([os.path.join(mydir, fn) for fn in os.listdir(mydir)]):
        fsents = xml2sents(ffn)
        all_fsents.update(fsents)
    with codecs.open(outf, "w", "utf8") as myout:
        for volnbr in sorted(all_fsents):
            print "Writing {}".format(volnbr)
            myout.write("\n".join(all_fsents[volnbr]) + "\n")


if __name__ == "__main__":
    main(indir)