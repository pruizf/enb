ENB 12
======

Scripts to crawl COP volumes in ENB volume 12, and format them as Solr-compatible XML besides plain text.

We used the corpus thus created for our [LREC 2016 paper](http://lrec-conf.org/proceedings/lrec2016/pdf/636_Paper.pdf)

Solr index configuration and Velocity templates also included (assumes an index called *enb12*). 

The clean corpus in text and XML is in the **out** directory.

A list with COP metadata (date and location) for each issue in vol 12 is in the **data** directory.

