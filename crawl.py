# -*- coding: utf8 -*-

"""
Crawls COP reports from ENB volume 12
Vol 12 URL: http://www.iisd.ca/vol12/
"""

import argparse
import chardet
import codecs
import inspect
import os
import re
import requests
import string
import sys
import urlparse


here = os.path.dirname(os.path.abspath(
    inspect.getfile(inspect.currentframe())))
sys.path.append(here)

import utils


# url patterns
URLPAT_TXT = "http://www.iisd.ca/download/asc/enb12{}e.txt"
URLPAT_HTML = "http://www.iisd.ca/vol12/enb12{}e.html"

# to store unprocessed content
RAWOUT = "/home/pablo/projects/clm/enbraw11000"

# problematic to decode: https://stackoverflow.com/questions/3628202
PROBLEM_LIST = ["\x80", "\x81", "\x82", "\x83", "\x84", "\x85",
                "\x86", "\x87", "\x88", "\x89", "\x8a", "\x8b",
                "\x8c", "\x8d", "\x8e", "\x8f",
                "\x90", "\x91", "\x92", "\x93", "\x94", "\x95",
                "\x96", "\x97", "\x98", "\x99", "\x9a", "\x9b",
                "\x9c", "\x9d", "\x9e", "\x9f"]

# these files need different treatment
decoding_exception_files = (618, 626)


def run_argparse():
    """CLI parser"""
    parser = argparse.ArgumentParser(description="Scrapes ENB corpus")
    parser.add_argument('outdir', metavar='DIR', help='Output directory')
    parser.add_argument('-r', '--rawdir', dest='rawdir', default=RAWOUT,
                        help='Output directory for raw ENB texts')
    return parser.parse_args()


def get_volume_content(volnbr, rawdir, redo=False):
    """
    Get raw text or html for a volume.
    @param redo: if True, recrawls files available in output dir
    @return:
      - The requested content if success
      - True if file for content already available in ouptut dir
        and redo is False
      - False if error
    @note: charset meta, requests' and chardet's encoding
    detection were imperfect, which lead to some of the operations below.
    Even browsers were not rendering some pages correctly (e.g. vol 543).
    """
    # After vol 295, only html available. Vol 120 is an exception
    if volnbr <= 295 and volnbr != 120:
        url = URLPAT_TXT.format(string.zfill(volnbr, 2))
    else:
        url = URLPAT_HTML.format(string.zfill(volnbr, 2))
    # skip if file already crawled
    if not redo:
        if os.path.exists(os.path.join(rawdir, os.path.split(
                urlparse.urlparse(url).path)[1])):
            print "  = Skipping: {}".format(url)
            return True
    print "- Getting text: {}".format(url)
    try:
        r = requests.get(url)
        if r.status_code == 404:
            print "! Error 404 with {}".format(url)
            return False
        else:
            badenc = False
            for c in PROBLEM_LIST:
                if c in r.content:
                    if (chardet.detect(r.content)["encoding"] == "utf-8" or
                        volnbr in decoding_exception_files):
                        mycontent = r.content.replace(
                            # invalid continuation byte
                            "\xe2\x80\xef\xbf\xbd", "\xe2\x80\x99")
                        mytext = mycontent.decode("utf8")
                    else:
                        mytext = r.content.decode("cp1252")
                    badenc = True
                    break
            if not badenc:
                mytext = r.text
            # about 20 reports affected by this correction
            mytext = utils.correct_other_text_errors(mytext)
            return mytext
    except UnicodeDecodeError as e:
        print "! Error with url {}: [{}]".format(url, e)
        return False


def write_volume_content(vn, txt, odir):
    """
    Write raw texts to file
    @param vn: volume number
    @param txt: text
    @param odir: output directory
    """
    if not os.path.exists(odir):
        os.makedirs(odir)
    # after vol 295, only html available
    # 120 is an exception
    if vn <= 295 and vn != 120:
        outfn = "enb12{}e.txt".format(string.zfill(vn, 2))
    else:
        outfn = "enb12{}e.html".format(string.zfill(vn, 2))
    if os.path.exists(os.path.join(odir, outfn)):
        print "  = Skipping {}".format(outfn)
        return
    print "  > Writing {}".format(outfn)
    with codecs.open(os.path.join(odir, outfn), "w", "utf8") as outff:
        # bad html in these files, otherwise BsP won't reach text
        if vn in (311, 315):
            txt = re.sub(r"</span></b>\s*(?:</font>)?", "  ", txt)
        outff.write(txt)


def main():
    v2txt = {}
    argus = run_argparse()
    v2c = utils.find_cop_name(utils.VOLS)
    if not os.path.exists(argus.outdir):
        os.makedirs(argus.outdir)
    print "Output directory: {}".format(argus.outdir)
    for vn in sorted(v2c):
        v2txt[vn] = get_volume_content(vn, argus.rawdir)
        if isinstance(v2txt[vn], bool):
            continue
        write_volume_content(vn, v2txt[vn], argus.rawdir)


if __name__ == "__main__":
    main()

